package behavioural.interpreter;

public class InterpreterPatternDemo {
	public static void main(String[] args){
		Expression isMale = getMaleExpression();
		Expression isMarriedWoman = getMarriedWomanExpression();
		
		System.out.println("John is male? "+ isMale.interpret("John"));
		System.out.println("Julie is a married woman? "+ isMarriedWoman.interpret("Julie Married"));
	}
	
	/*
	 * Creating rules giving expressions data,
	 * which is required in the context to make the
	 * expression true.
	 */
	
	//Rule: Robert and John are male
	private static Expression getMaleExpression(){
		Expression robert = new TerminalExpression("Robert");
		Expression john = new TerminalExpression("John");
		return new OrExpression(robert, john);
	}
	
	//Rule: Julie is a married women
	private static Expression getMarriedWomanExpression(){
		Expression julie = new TerminalExpression("Julie");
		Expression married = new TerminalExpression("Married");
		return new AndExpression(julie, married);
	}
}

interface Expression{
	public boolean interpret(String context);
}

class TerminalExpression implements Expression{
	//Piece of data required in context to make expression true.
	private String data;
	
	public TerminalExpression(String data){
		this.data = data;
	}
	
	@Override
	public boolean interpret(String context) {
		if(context.contains(data)){
			return true;
		}
		return false;
	}
}

class OrExpression implements Expression{
	private Expression expr1;
	private Expression expr2;
	
	public OrExpression(Expression expr1, Expression expr2){
		this.expr1 = expr1;
		this.expr2 = expr2;
	}
	
	@Override
	public boolean interpret(String context) {
		return expr1.interpret(context) || expr2.interpret(context);
	}
}

class AndExpression implements Expression{
	private Expression expr1;
	private Expression expr2;
	
	public AndExpression(Expression expr1, Expression expr2){
		this.expr1 = expr1;
		this.expr2 = expr2;
	}
	
	@Override
	public boolean interpret(String context) {
		return expr1.interpret(context) && expr2.interpret(context);
	}
}