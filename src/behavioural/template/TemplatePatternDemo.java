package behavioural.template;

public class TemplatePatternDemo {
	public static void main(String[] args){
		Game game = new Circket();
		game.play();
		
		System.out.println();
		
		game = new Football();
		game.play();
	}
}

abstract class Game{
	abstract void intialize();
	abstract void startPlay();
	abstract void endPlay();
	
	//template method
	public final void play(){
		intialize();
		startPlay();
		endPlay();
	}
}

class Circket extends Game{

	@Override
	void intialize() {
		System.out.println("Cricket Game Initialized! Start playing.");
	}

	@Override
	void startPlay() {
		System.out.println("Cricket Game Started. Enjoy the game!");
	}

	@Override
	void endPlay() {
		System.out.println("Cricket Game Finished!");
	}
}

class Football extends Game{

	@Override
	void intialize() {
		System.out.println("Football Game Initialized! Start playing.");
	}

	@Override
	void startPlay() {
		System.out.println("Football Game Started. Enjoy the game!");
	}

	@Override
	void endPlay() {
		System.out.println("Football Game Finished!");
	}
}