package behavioural.iterator;

public class IteratorPatternDemo {
	public static void main(String[] args){
		NameRespository namesRepo = new NameRespository();
		
		//Loops through every item in list of unknown size/structure.
		for(Iterator iter = namesRepo.getIterator(); iter.hasNext();){
			String name = (String)iter.next();
			System.out.println("Name "+name);
		}
	}
}

interface Iterator{
	public boolean hasNext();
	public Object next();
}

interface Container{
	public Iterator getIterator();
}

class NameRespository implements Container{
	public String names[] = {"Callum", "Chris", "Joseph", "Sam", "Joc"};
	
	@Override
	public Iterator getIterator(){
		return new NameIterator();
	}
	
	private class NameIterator implements Iterator{
		int index;

		@Override
		public boolean hasNext() {
			if(index < names.length){
				return true;
			}
			return false;
		}

		@Override
		public Object next() {
			if(this.hasNext()){
				return names[index++];
			}
			return null;
		}
	}
}