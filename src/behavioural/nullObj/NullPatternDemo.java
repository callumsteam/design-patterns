package behavioural.nullObj;

public class NullPatternDemo {
	public static void main(String[] args){
		AbstractCustomer customer1 = CustomerFactory.getCustomer("Callum");
		AbstractCustomer customer2 = CustomerFactory.getCustomer("Caymen");
		AbstractCustomer customer3 = CustomerFactory.getCustomer("Liz");
		AbstractCustomer customer4 = CustomerFactory.getCustomer("Joe");
		
		System.out.println("Customers:");
		System.out.println(customer1.getName());
		System.out.println(customer2.getName());
		System.out.println(customer3.getName());
		System.out.println(customer4.getName());
	}
}

abstract class AbstractCustomer{
	protected String name;
	public abstract boolean isNil();
	public abstract String getName();
}

class RealCustomer extends AbstractCustomer{
	public RealCustomer(String name){
		this.name = name;
	}
	
	@Override
	public String getName(){
		return name;
	}
	
	@Override
	public boolean isNil(){
		return false;
	}
}

class NullCustomer extends AbstractCustomer{
	@Override
	public String getName(){
		return "Not available in customer database";
	}
	
	@Override
	public boolean isNil(){
		return true;
	}
}

class CustomerFactory{
	public static final String[] names = {"Callum", "Caymen", "Liz"};
	
	//Returns a NullCustomer instead of 'null' if customer doesn't exist.
	public static AbstractCustomer getCustomer(String name){
		for(String listedName : names){
			if(listedName.equalsIgnoreCase(name)){
				return new RealCustomer(listedName);
			}
		}
		return new NullCustomer();
	}
}