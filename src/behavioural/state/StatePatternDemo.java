package behavioural.state;

public class StatePatternDemo {
	public static void main(String[] args){
		Context context = new Context();
		
		StartState startState = new StartState();
		startState.doAction(context);
		
		System.out.println(context.getStateString());
		
		StopState stopState = new StopState();
		stopState.doAction(context);
		
		System.out.println(context.getStateString());
	}
}

/*
 * Context object uses State objects methods,
 * to change behavior depending on the state that is set.
 */
class Context{
	private State state;
	
	public Context(){
		state = null;
	}
	
	public void setState(State state){
		this.state = state;
	}
	
	public String getStateString(){
		return state.toString();
	}
}

interface State{
	public void doAction(Context context);
}

class StartState implements State{
	
	@Override
	public void doAction(Context context){
		System.out.println("Player is in start state");
		context.setState(this);
	}
	
	public String toString(){
		return "Start state";
	}
}

class StopState implements State{
	
	@Override
	public void doAction(Context context){
		System.out.println("Player is in stop state");
		context.setState(this);
	}
	
	public String toString(){
		return "Stop state";
	}
}