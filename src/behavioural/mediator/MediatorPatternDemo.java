package behavioural.mediator;

import java.util.Date;

public class MediatorPatternDemo {
	public static void main(String[] args){
		ChatRoom chatRoom = new ChatRoom();
		
		User harry = new User("Harry", chatRoom);
		User john = new User("John", chatRoom);
		
		harry.sendMessage("Hi John!");
		john.sendMessage("Hello Harry.");
	}
}

//Mediator class, used to reduce communication between objects.
class ChatRoom{
	public void showMessage(User user, String message){
		System.out.println(new Date().toString() + " [" + user.getName() + "] : "+ message);
	}
}

class User{
	private String name;
	private ChatRoom chatRoom;
	
	public User(String name, ChatRoom chatRoom){
		this.name = name;
		this.chatRoom = chatRoom;
	}
	
	public String getName(){
		return name;
	}
	
	public void sendMessage(String message){
		chatRoom.showMessage(this, message);
	}
}