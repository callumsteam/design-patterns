package behavioural.chain;

public class ChainPatternDemo {
	public static void main(String[] args){
		AbstractLogger loggerChain = getChainOfLoggers();
		
		loggerChain.logMessage(LoggerLevels.INFO, "This is an info message");
		loggerChain.logMessage(LoggerLevels.DEBUG, "This is a debug message");
		loggerChain.logMessage(LoggerLevels.ERROR, "This is an error message");
	}
	
	private static AbstractLogger getChainOfLoggers(){
		AbstractLogger errorLogger = new ErrorLogger(LoggerLevels.ERROR);
		AbstractLogger debugLogger = new DebugLogger(LoggerLevels.DEBUG);
		AbstractLogger infoLogger = new InfoLogger(LoggerLevels.INFO);
		
		errorLogger.setNextLogger(debugLogger);
		debugLogger.setNextLogger(infoLogger);
		
		return errorLogger;
	}
}

enum LoggerLevels{
	INFO,
	DEBUG,
	ERROR
}

abstract class AbstractLogger{
	
	protected LoggerLevels level;
	
	//Next element in chain.
	protected AbstractLogger nextLogger;
	
	public void setNextLogger(AbstractLogger logger){
		this.nextLogger = logger;
	}
	
	public void logMessage(LoggerLevels level, String message){
		if(this.level == level){
			write(message);
		}
		/*
		 * This element in chain can't deal with request,
		 * passing request onto next element in chain.
		 */
		else if(nextLogger != null){
			nextLogger.logMessage(level, message);
		}
	}
	
	abstract protected void write(String message);
}

class InfoLogger extends AbstractLogger{
	public InfoLogger(LoggerLevels level){
		this.level = level;
	}

	@Override
	protected void write(String message) {
		System.out.println("\nStandard logger - "+message);
	}
}
class DebugLogger extends AbstractLogger{
	public DebugLogger(LoggerLevels level){
		this.level = level;
	}
	
	@Override
	protected void write(String message) {
		System.out.println("\nDebug logger - "+message);
	}
}

class ErrorLogger extends AbstractLogger{
	public ErrorLogger(LoggerLevels level){
		this.level = level;
	}

	@Override
	protected void write(String message) {
		System.err.println("\nError logger - "+message);
	}
}
