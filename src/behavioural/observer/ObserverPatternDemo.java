package behavioural.observer;

import java.util.ArrayList;
import java.util.List;

public class ObserverPatternDemo {
	public static void main(String[] args){
		Subject subject = new Subject();
		
		new BinaryObserver(subject);
		new OctalObserver(subject);
		new HexaObserver(subject);
		
		System.out.println("First state change (15)");
		subject.setState(15);
		
		System.out.println("\nSecond state change (10)");
		subject.setState(10);
	}
}

/*
 * Subject holds a state value.
 * When the state value changes, it calls
 * the update method on all of it's observers.
 */
class Subject{
	private List<Observer> observers = new ArrayList<Observer>();
	private int state;
	
	public int getState(){
		return state;
	}
	
	public void setState(int state){
		this.state = state;
		notifyAllObservers();
	}
	
	public void attach(Observer observer){
		observers.add(observer);
	}
	
	private void notifyAllObservers(){
		for(Observer observer : observers){
			observer.update();
		}
	}
}

abstract class Observer{
	protected Subject subject;
	public abstract void update();
}

class BinaryObserver extends Observer{
	public BinaryObserver(Subject subject){
		this.subject = subject;
		this.subject.attach(this);
	}
	
	@Override
	public void update(){
		System.out.println("Binary string: " + Integer.toBinaryString(subject.getState()));
	}
}

class OctalObserver extends Observer{
	public OctalObserver(Subject subject){
		this.subject = subject;
		this.subject.attach(this);
	}
	
	@Override
	public void update(){
		System.out.println("Octal string: " + Integer.toOctalString(subject.getState()));
	}
}

class HexaObserver extends Observer{
	public HexaObserver(Subject subject){
		this.subject = subject;
		this.subject.attach(this);
	}
	
	@Override
	public void update(){
		System.out.println("Hexadecimal string: " + Integer.toHexString(subject.getState()));
	}
}