package behavioural.memento;

import java.util.ArrayList;
import java.util.List;

public class MementoPatternDemo {
	public static void main(String[] args){
		Originator originator = new Originator();
		CareTaker careTaker = new CareTaker();
		
		originator.setState("State #1");
		originator.setState("State #2");
		careTaker.add(originator.saveStateToMemento());
		
		originator.setState("State #3");
		careTaker.add(originator.saveStateToMemento());
		
		originator.setState("State #4");
		System.out.println("Current state: "+originator.getState());
		
		//Loads in previously saved states.
		originator.getStateFromMemento(careTaker.get(0));
		System.out.println("First saved state: "+originator.getState());
		originator.getStateFromMemento(careTaker.get(1));
		System.out.println("Second saved state: "+originator.getState());
	}
}

//Object to be saved, which holds state.
class Memento{
	private String state;
	
	public Memento(String state){
		this.state = state;
	}
	
	public String getState(){
		return state;
	}
}

//Object which holds a state in a Momento
class Originator{
	private String state;
	
	public void setState(String state){
		this.state = state;
	}
	
	public String getState(){
		return state;
	}
	
	public Memento saveStateToMemento(){
		return new Memento(state);
	}
	
	public void getStateFromMemento(Memento memento){
		state = memento.getState();
	}
}

//Stores list of saved Mementos
class CareTaker{
	private List<Memento> mementoList = new ArrayList<Memento>();
	
	public void add(Memento state){
		mementoList.add(state);
	}
	
	public Memento get(int index){
		return mementoList.get(index);
	}
}