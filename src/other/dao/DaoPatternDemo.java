package other.dao;

import java.util.ArrayList;
import java.util.List;

public class DaoPatternDemo {
	public static void main(String[] args){
		StudentDAO studentDao = new StudentDAOImpl();
		
		System.out.println("Getting all students: ");
		for(Student student : studentDao.getAllStudents()){
			System.out.println("Student: [Student ID: "+student.getStudentID()+", Name: "+student.getName()+" ]");
		}
		
		System.out.println("\nGetting student with ID - 0: ");
		Student student = studentDao.getStudentByID(0);
		System.out.println("Student: [Student ID: "+student.getStudentID()+", Name: "+student.getName()+" ]");
		
		System.out.println("\nUpdating student 0's name: ");
		student.setName("Mike");
		studentDao.updateStudent(student);
		System.out.println("Student: [Student ID: "+student.getStudentID()+", Name: "+student.getName()+" ]");
	}
}

class Student{
	private String name;
	private int studentID;
	
	Student(String name, int studentID){
		this.name = name;
		this.studentID = studentID;
	}

	public String getName() {
		return name;
	}

	public int getStudentID() {
		return studentID;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setStduentID(int studentID) {
		this.studentID = studentID;
	}
}

interface StudentDAO{
	public List<Student> getAllStudents();
	public Student getStudentByID(int studentID);
	public void updateStudent(Student student);
	public void deleteStudent(Student student);
}

class StudentDAOImpl implements StudentDAO{
	//List represents a database.
	List<Student> students;
	
	public StudentDAOImpl(){
		students = new ArrayList<Student>();
		students.add(new Student("Callum", 0));
		students.add(new Student("Billy", 1));
	}

	@Override
	public List<Student> getAllStudents() {
		return students;
	}

	@Override
	public Student getStudentByID(int studentID) {
		return students.get(studentID);
	}

	@Override
	public void updateStudent(Student student) {
		students.get(student.getStudentID()).setName(student.getName());
		System.out.println("Student: Student ID - "+student.getStudentID()+", updated in the database");
	}

	@Override
	public void deleteStudent(Student student) {
		students.remove(student.getStudentID());
		System.out.println("Student: Student ID - "+student.getStudentID()+", deleted from database");
	}
}