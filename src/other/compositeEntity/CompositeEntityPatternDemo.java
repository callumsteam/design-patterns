package other.compositeEntity;

public class CompositeEntityPatternDemo {
	public static void main(String[] args){
		Client client = new Client();
		client.setData("Test", "Data");
		client.printData();
		
		System.out.println();
		
		client.setData("Second Test", "Data1");
		client.printData();
	}
}

class DependantObject1{
	private String data;
	
	public void setData(String data){
		this.data = data;
	}
	
	public String getData(){
		return data;
	}
}

class DependantObject2{
	private String data;
	
	public void setData(String data){
		this.data = data;
	}
	
	public String getData(){
		return data;
	}
}

//Holds many data objects.
class CoarseGrainedObject{
	DependantObject1 do1 = new DependantObject1();
	DependantObject2 do2 = new DependantObject2();
	
	public void setData(String data1, String data2){
		do1.setData(data1);
		do2.setData(data2);
	}
	
	//Collates data from multiple objects into one array.
	public String[] getData(){
		return new String[] {do1.getData(), do2.getData()};
	}
}

class CompositeEntity{
	private CoarseGrainedObject cgo = new CoarseGrainedObject();
	
	public void setData(String data1, String data2){
		cgo.setData(data1, data2);
	}
	
	public String[] getData(){
		return cgo.getData();
	}
}

class Client{
	private CompositeEntity compositeEntity = new CompositeEntity();
	
	public void printData(){
		for(String data : compositeEntity.getData()){
			System.out.println("Data: "+data);
		}
	}
	
	public void setData(String data1, String data2){
		compositeEntity.setData(data1, data2);
	}
}