package other.frontController;

public class FrontControllerPatternDemo {
	public static void main(String[] args){
		FrontController frontController = new FrontController();
		
		frontController.dispatcheReuest("HOME");
		
		System.out.println();
		
		frontController.dispatcheReuest("STUDENT");
	}
}

class HomeView{
	public void show(){
		System.out.println("Displaying home page");
	}
}

class StudentView{
	public void show(){
		System.out.println("Displaying student page");
	}
}

class Dispatcher{
	private StudentView studentView;
	private HomeView homeView;
	
	public Dispatcher(){
		studentView = new StudentView();
		homeView = new HomeView();
	}
	
	public void dispatch(String request){
		switch(request.toUpperCase()){
		case "STUDENT":
			studentView.show();
			break;
			
		case "HOME":
		default:
			homeView.show();
			break;
		}
	}
}

class FrontController{
	private Dispatcher dispatcher;
	
	public FrontController(){
		dispatcher = new Dispatcher();
	}
	
	private boolean isAuthenticUser(){
		System.out.println("User is authenticated successfully.");
		return true;
	}
	
	private void trackRequest(String request){
		System.out.println("Page requested: "+request);
	}
	
	public void dispatcheReuest(String request){
		//audit trail
		trackRequest(request);
		
		if(isAuthenticUser()){
			dispatcher.dispatch(request);
		}
	}
}