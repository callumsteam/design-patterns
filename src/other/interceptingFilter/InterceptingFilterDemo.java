package other.interceptingFilter;

import java.util.ArrayList;
import java.util.List;

public class InterceptingFilterDemo {
	public static void main(String[] args){
		FilterManager filterManager = new FilterManager(new Target());
		filterManager.setFilter(new AuthenicationFilter());
		filterManager.setFilter(new DebugFilter());
		
		Client client = new Client();
		client.setFilterManager(filterManager);
		client.sendRequest("REQUEST");
	}
}

interface Filter{
	public void execute(String request);
}

class AuthenicationFilter implements Filter{
	@Override
	public void execute(String request){
		System.out.println("Authenticating request: "+request);
	}
}

class DebugFilter implements Filter{
	@Override
	public void execute(String request){
		System.out.println("Request log: "+request);
	}
}

class Target{
	public void execute(String request){
		System.out.println("Executing request: "+request);
	}
}

//Controls the order in which filters are applied.
class FilterChain{
	private List<Filter> filters = new ArrayList<Filter>();
	private Target target;
	
	public FilterChain(Target target){
		this.target = target;
	}
	
	public void addFilter(Filter filter){
		filters.add(filter);
	}
	
	public void execute(String request){
		for(Filter filter : filters){
			filter.execute(request);
		}
		target.execute(request);
	}
}

//FilterChain wrapper
class FilterManager{
	FilterChain filterChain;
	
	public FilterManager(Target target){
		filterChain = new FilterChain(target);
	}
	
	public void setFilter(Filter filter){
		filterChain.addFilter(filter);
	}
	
	public void filterRequest(String request){
		filterChain.execute(request);
	}
}

//Object which sends requests
class Client{
	FilterManager filterManager;
	
	public void setFilterManager(FilterManager filterManager){
		this.filterManager = filterManager;
	}
	
	public void sendRequest(String request){
		filterManager.filterRequest(request);
	}
}