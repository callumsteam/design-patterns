
package other.serviceLocator;

import java.util.ArrayList;
import java.util.List;

public class ServiceLocatorPatternDemo {
	public static void main(String[] args){
		Service service = ServiceLocator.getService("Service1");
		service.execute();
		service = ServiceLocator.getService("Service2");
		service.execute();
		
		System.out.println();
		//Cached services
		service = ServiceLocator.getService("Service1");
		service.execute();
		service = ServiceLocator.getService("Service2");
		service.execute();
	}
}

interface Service{
	public String getName();
	public void execute();
}

class Service1 implements Service{
	@Override
	public void execute(){
		System.out.println("Executing Service1");
	}
	
	@Override
	public String getName(){
		return "Service1";
	}
}

class Service2 implements Service{
	@Override
	public void execute(){
		System.out.println("Executing Service2");
	}
	
	@Override
	public String getName(){
		return "Service2";
	}
}

class InitialContext{
	public Object lookup(String jndiName){
		switch(jndiName.toUpperCase()){
			case "SERVICE1":
				System.out.println("Looking up and creating new Service1 object.");
				return new Service1();
				
			case "SERVICE2":
				System.out.println("Looking up and creating new Service2 object.");
				return new Service2();
				
			default:
				return null;
		}
	}
}

class Cache{
	private List<Service> services;
	
	public Cache(){
		services = new ArrayList<Service>();
	}
	
	public Service getService(String serviceName){
		for(Service service : services){
			if(service.getName().equalsIgnoreCase(serviceName)){
				System.out.println("Returning cached "+service.getName()+" object.");
				return service;
			}
		}
		
		return null;
	}
	
	public void addService(Service newService){
		//Checks service isn't already cached.
		for(Service service : services){
			if(service.getName().equalsIgnoreCase(newService.getName())){
				return;
			}
		}
		
		services.add(newService);
	}
}

class ServiceLocator{
	private static Cache cache = new Cache();
	
	public static Service getService(String jndiName){
		Service service = cache.getService(jndiName);
		
		if(service!= null){
			return service;
		}
		
		InitialContext context = new InitialContext();
		service = (Service)context.lookup(jndiName);
		cache.addService(service);
		return service;
	}
}