package other.transferObject;

import java.util.ArrayList;
import java.util.List;


public class TransferObjectPatternDemo {
	public static void main(String[] args){
		StudentBO studentBo = new StudentBO();
		
		System.out.println("Getting all students: ");
		for(StudentVO student : studentBo.getAllStudents()){
			System.out.println("Student: [Student ID: "+student.getStudentID()+", Name: "+student.getName()+" ]");
		}
		
		System.out.println("\nGetting student with ID - 0: ");
		StudentVO student = studentBo.getStudentByID(0);
		System.out.println("Student: [Student ID: "+student.getStudentID()+", Name: "+student.getName()+" ]");
		
		System.out.println("\nUpdating student 0's name: ");
		student.setName("Mike");
		studentBo.updateStudent(student);
		System.out.println("Student: [Student ID: "+student.getStudentID()+", Name: "+student.getName()+" ]");
	}
}

class StudentVO{
	private String name;
	private int studentID;
	
	public StudentVO(String name, int studentID){
		this.name = name;
		this.studentID = studentID;
	}

	public String getName() {
		return name;
	}

	public int getStudentID() {
		return studentID;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setStudentID(int studentID) {
		this.studentID = studentID;
	}
}

class StudentBO{
	//List represents a database.
	List<StudentVO> students;
	
	public StudentBO(){
		students = new ArrayList<StudentVO>();
		students.add(new StudentVO("Callum", 0));
		students.add(new StudentVO("Billy", 1));
	}

	public List<StudentVO> getAllStudents() {
		return students;
	}

	public StudentVO getStudentByID(int studentID) {
		return students.get(studentID);
	}

	public void updateStudent(StudentVO student) {
		students.get(student.getStudentID()).setName(student.getName());
		System.out.println("Student: Student ID - "+student.getStudentID()+", updated in the database");
	}


	public void deleteStudent(StudentVO student) {
		students.remove(student.getStudentID());
		System.out.println("Student: Student ID - "+student.getStudentID()+", deleted from database");
	}
}