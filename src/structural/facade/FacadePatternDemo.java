package structural.facade;

public class FacadePatternDemo {
	public static void main(String[] args){
		ShapeMaker shapeMaker = new ShapeMaker();
		
		shapeMaker.drawCircle();
		shapeMaker.drawSquare();
		shapeMaker.drawRectangle();
	}
}

interface Shape{
	void draw();
}

class Rectangle implements Shape{

	@Override
	public void draw() {
		System.out.println("Rectangle::draw()");
	}
}

class Circle implements Shape{

	@Override
	public void draw() {
		System.out.println("Circle::draw()");
	}
}

class Square implements Shape{

	@Override
	public void draw() {
		System.out.println("Square::draw()");
	}
}

//Offers simpler methods for functionality in system.
class ShapeMaker{
	private Shape circle;
	private Shape square;
	private Shape rectangle;
	
	public ShapeMaker(){
		circle = new Circle();
		square = new Square();
		rectangle = new Rectangle();
	}
	
	public void drawCircle(){
		circle.draw();
	}
	
	public void drawSquare(){
		square.draw();
	}
	
	public void drawRectangle(){
		rectangle.draw();
	}
}