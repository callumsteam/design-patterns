package structural.proxy;

public class ProxyPatternDemo {
	public static void main(String[] args){
		Image image = new ProxyImage("folder/images/coolPic.png");
		
		//Will be loaded from disk.
		image.display();
		System.out.println("");
		
		//Instance in memory used, instead of loading from disk.
		image.display();
	}
}

interface Image{
	void display();
}

class RealImage implements Image{
	private String file;
	
	public RealImage(String file){
		this.file = file;
		load();
	}
	
	private void load(){
		System.out.println("Loading "+file);
	}
	
	@Override
	public void display() {
		System.out.println("Displaying "+file);
	}
}

class ProxyImage implements Image{
	private RealImage realImage;
	private String file;
	
	public ProxyImage(String file){
		this.file = file;
	}
	
	@Override
	public void display() {
		if(realImage == null){
			realImage = new RealImage(file);
		}
		
		realImage.display();
	}	
}