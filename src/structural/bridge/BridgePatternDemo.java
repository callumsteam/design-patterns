package structural.bridge;

/*
 * Another example of the bridge pattern could be used within a system that sends messages, 
 * perhaps describing system status changes, warnings and errors. 
 * An abstraction class that represents a message could include properties to hold the details of the message. 
 * Several implementations could then be included to permit sending the messages via email, over a messaging queue or to a web service. 
 * The implementation to be used could then be selected according to the availability of these resources.
 */

public class BridgePatternDemo {
	public static void main(String[] args){
		Shape redCircle = new Circle(100, 100, 10, new RedCircle());
		Shape greenCircle = new Circle(100, 100, 10, new GreenCircle());
		
		redCircle.draw();
		greenCircle.draw();
	}
}

interface DrawAPI{
	public void drawCircle(int radius, int x, int y);
}

class RedCircle implements DrawAPI{

	@Override
	public void drawCircle(int radius, int x, int y) {
		System.out.println("Drawing Circle[ color: red, radius: " + radius + ", x: " + x + ", y: " + y + "]");
	}
}

class GreenCircle implements DrawAPI{

	@Override
	public void drawCircle(int radius, int x, int y) {
		System.out.println("Drawing Circle[ color: green, radius: " + radius + ", x: " + x + ", y: " + y + "]");
	}
}

abstract class Shape{
	/*
	 * This means the Shape class can draw the shape
	 * however way is required, as long as it's an impl of DrawAPI.
	 */
	protected DrawAPI drawAPI;
	
	protected Shape(DrawAPI drawAPI){
		this.drawAPI = drawAPI;
	}
	
	public abstract void draw();
}

class Circle extends Shape{
	private int x, y, radius;
	
	public Circle(int x, int y, int radius, DrawAPI drawAPI){
		super(drawAPI);
		this.x = x;
		this.y = y;
		this.radius = radius;
	}
	
	@Override
	public void draw(){
		//Drawing of shape abstrated away from class, to DrawAPI.
		drawAPI.drawCircle(radius, x, y);
	}
}