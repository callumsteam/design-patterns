package structural.flyweight;

import java.util.HashMap;

/*
 * Caches previously made object,
 * then returns the previously made object if exists.
 * Used to reduce use of memory.
 */
public class FlyweightPatternDemo {
	private static final String[] colours = {"Red", "Green", "Blue", "Orange", "Purple"};
	
	public static void main(String[] args){
		for(int i = 0; i < 20 ; i++){
			Circle circle = ShapeFactory.getCircle(getRandomColour());
			circle.setX(getRandomXY());
			circle.setY(getRandomXY());
			circle.setRadius(10);
			circle.draw();
		}
	}
	
	private static String getRandomColour(){
		return colours[(int)(Math.random() * colours.length)];
	}
	
	private static int getRandomXY(){
		return (int)(Math.random() * 100);
	}
}

interface Shape{
	public void draw();
}

class Circle implements Shape{
	private String colour;
	private int x;
	private int y;
	private int radius;
	
	public Circle(String colour){
		this.colour = colour;
	}

	public void setX(int x) {
		this.x = x;
	}

	public void setY(int y) {
		this.y = y;
	}

	public void setRadius(int radius) {
		this.radius = radius;
	}

	@Override
	public void draw() {
		System.out.println("Circle: Draw() [Colour : " + colour + ", x : " + x + ", y :" + y + ", radius :" + radius);
	}
}

class ShapeFactory{
	//Holds previously created objects with key.
	private static final HashMap<String, Circle> circleMap = new HashMap<String, Circle>();
	
	/*
	 * Searches map for Circle of right colour.
	 * Returns one in hashmap if it exists,
	 * otherwise creates new one and adds to map.
	 */
	public static Circle getCircle(String colour){
		Circle circle = circleMap.get(colour);
		
		if(circle == null){
			circle = new Circle(colour);
			circleMap.put(colour, circle);
			System.out.println("Creating circle of colour - "+colour);
		}
		
		return circle;
	}
}