package structural.adapter;

public class AdapterPatternDemo {
	public static void main(String[] args){
		AudioPlayer audioPlayer = new AudioPlayer();
		
		audioPlayer.play("mp3", "Song1.mp3");
		audioPlayer.play("mp4", "Song2.mp4");
		audioPlayer.play("vlc", "Song3.vlc");
		audioPlayer.play("mov", "Song4.mov");
	}
}

interface MediaPlayer{
	public void play(String audioType, String fileName);
}

interface AdvancedMediaPlayer{
	public void playVlc(String fileName);
	public void playMp4(String fileName);
}

class VlcPlayer implements AdvancedMediaPlayer{

	@Override
	public void playVlc(String fileName) {
		System.out.println("Playing vlc file. Name: "+fileName);
		
	}

	@Override
	public void playMp4(String fileName) {
		// TODO Auto-generated method stub
		
	}
}

class Mp4Player implements AdvancedMediaPlayer{

	@Override
	public void playVlc(String fileName) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void playMp4(String fileName) {
		System.out.println("Playing mp4 file. Name: "+fileName);
		
	}
}

class MediaAdapter implements MediaPlayer{

	AdvancedMediaPlayer advancedMediaPlayer;
	
	public MediaAdapter(String audioType){
		if(audioType.equalsIgnoreCase("vlc"))
		{
			advancedMediaPlayer = new VlcPlayer();
		}
		else if(audioType.equalsIgnoreCase("mp4"))
		{
			advancedMediaPlayer = new Mp4Player();
		}
	}
	
	@Override
	/*
	 * Adapts the MediaPlayer interfaces classes, to work
	 * within a class implementing the AdvancedMediaPlayer interface.
	 */
	
	
	public void play(String audioType, String fileName) {
		if(audioType.equalsIgnoreCase("vlc"))
		{
			advancedMediaPlayer.playVlc(fileName);
		}
		else if(audioType.equalsIgnoreCase("mp4"))
		{
			advancedMediaPlayer.playMp4(fileName);
		}
	}
}

class AudioPlayer implements MediaPlayer{
	
	@Override
	public void play(String audioType, String fileName) {
		//inbuilt support for mp3 file.
		if(audioType.equalsIgnoreCase("mp3"))
		{
			System.out.println("Playing mp3 file. Name: "+fileName);
		}
		else if(audioType.equalsIgnoreCase("vlc") || audioType.equalsIgnoreCase("mp4"))
		{
			MediaAdapter mediaAdaptor = new MediaAdapter(audioType);
			mediaAdaptor.play(audioType, fileName);
		}
		else
		{
			System.out.println("Invalid media. " + audioType + " format not supported");
		}
	}
}