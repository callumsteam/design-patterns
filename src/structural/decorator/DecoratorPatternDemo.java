package structural.decorator;

public class DecoratorPatternDemo {
	public static void main(String[] args){
		Shape circle = new Circle();
		Shape redCircle = new RedDecorator(circle);
		Shape redRectangle = new RedDecorator(new Rectangle());
		
		System.out.println("Circle with normal border: ");
		circle.draw();
		
		System.out.println("\nCircle with red border: ");
		redCircle.draw();
		
		System.out.println("\nRectangle with red border: ");
		redRectangle.draw();
	}
}

interface Shape{
	void draw();
}

class Rectangle implements Shape{

	@Override
	public void draw() {
		System.out.println("Shape: Rectangle");
	}
}

class Circle implements Shape{

	@Override
	public void draw() {
		System.out.println("Shape: Circle");
	}
}

//Wrapper class for Shape interface.
abstract class ShapeDecorator implements Shape{
	protected Shape decoratedShape;
	
	public ShapeDecorator(Shape decoratedShape){
		this.decoratedShape = decoratedShape;
	}
	
	public abstract void draw();
}

//Decorator class adds extra functionality to object.
class RedDecorator extends ShapeDecorator{
	public RedDecorator(Shape decoratedShape){
		super(decoratedShape);
	}
	
	@Override
	public void draw(){
		decoratedShape.draw();
		setRedBorder(decoratedShape);
	}
	
	private void setRedBorder(Shape decoratedShape){
		System.out.println("Border colour: Red");
	}
}