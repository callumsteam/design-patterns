package structural.filter;

import java.util.ArrayList;
import java.util.List;

public class CriteriaPatternDemo {
	public static void main(String[] args){
		List<Person> persons = new ArrayList<Person>();
		
		persons.add(new Person("Callum", "Male", "Married"));
		persons.add(new Person("Sammy", "Female", "Married"));
		persons.add(new Person("Peter", "Male", "Single"));
		persons.add(new Person("Matt", "Male", "Married"));
		persons.add(new Person("Caymen", "Female", "Married"));
		persons.add(new Person("Billy", "Male", "Single"));
		
		Criteria male = new CriteriaMale();
		Criteria female = new CriteriaFemale();
		Criteria single = new CriteriaSingle();
		Criteria singleMale = new AndCriteria(male, single);
		Criteria singleOrFemale = new OrCriteria(single, female);
		
		System.out.println("Males: ");
		printPersons(male.meetCriteria(persons));
		
		System.out.println("\nFemales: ");
		printPersons(female.meetCriteria(persons));
		
		System.out.println("\nSingle males: ");
		printPersons(singleMale.meetCriteria(persons));
		
		System.out.println("\nSingle or female: ");
		printPersons(singleOrFemale.meetCriteria(persons));
	}
	
	private static void printPersons(List<Person> persons){
		for(Person person : persons){
			System.out.println("Person : [ Name : " + person.getName() + ", Gender : " + person.getGender() + ", Marital Status : " + person.getMaritalStatus() + " ]");
		}
	}
}

class Person{
	private String name;
	private String gender;
	private String maritalStatus;
	
	public Person(String name, String gender, String maritalStatus){
		this.name = name;
		this.gender = gender;
		this.maritalStatus = maritalStatus;
	}
	
	public String getName() {
		return name;
	}
	
	public String getGender() {
		return gender;
	}
	
	public String getMaritalStatus() {
		return maritalStatus;
	}
}

interface Criteria{
	public List<Person> meetCriteria(List<Person> persons);
}

class CriteriaMale implements Criteria{

	@Override
	public List<Person> meetCriteria(List<Person> persons) {
		List<Person> malePersons = new ArrayList<Person>();
		
		for(Person person : persons){
			if(person.getGender().equalsIgnoreCase("MALE")){
				malePersons.add(person);
			}
		}
		
		return malePersons;
	}
}

class CriteriaFemale implements Criteria{

	@Override
	public List<Person> meetCriteria(List<Person> persons) {
		List<Person> femalePersons = new ArrayList<Person>();
		
		for(Person person : persons){
			if(person.getGender().equalsIgnoreCase("FEMALE")){
				femalePersons.add(person);
			}
		}
		
		return femalePersons;
	}
}

class CriteriaSingle implements Criteria{

	@Override
	public List<Person> meetCriteria(List<Person> persons) {
		List<Person> singlePersons = new ArrayList<Person>();
		
		for(Person person : persons){
			if(person.getMaritalStatus().equalsIgnoreCase("SINGLE")){
				singlePersons.add(person);
			}
		}
		
		return singlePersons;
	}
}

class AndCriteria implements Criteria{

	private Criteria criteria;
	private Criteria otherCriteria;
	
	public AndCriteria(Criteria criteria, Criteria otherCriteria){
		this.criteria = criteria;
		this.otherCriteria = otherCriteria;
	}
	
	@Override
	public List<Person> meetCriteria(List<Person> persons) {
		return otherCriteria.meetCriteria(criteria.meetCriteria(persons));
	}
}

class OrCriteria implements Criteria{

	private Criteria criteria;
	private Criteria otherCriteria;
	
	public OrCriteria(Criteria criteria, Criteria otherCriteria){
		this.criteria = criteria;
		this.otherCriteria = otherCriteria;
	}
	
	@Override
	public List<Person> meetCriteria(List<Person> persons) {
		List<Person> criteriaList = criteria.meetCriteria(persons);
		List<Person> otherCriteriaList = otherCriteria.meetCriteria(persons);
		
		for(Person person : otherCriteriaList){
			if(!criteriaList.contains(person)){
				criteriaList.add(person);
			}
		}
		
		return criteriaList;
	}
}