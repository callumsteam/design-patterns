package structural.composite;

import java.util.ArrayList;
import java.util.List;

/*
 * Composite pattern creates a hierarchy tree structure,
 * by letting objects have a property bring a list of objects of its own type.
 */
public class CompositePatternDemo {
	public static void main(String[] args){
		Employee CEO = new Employee("Grampa", "CEO", 27000);
		
		Employee headSales = new Employee("Homer", "Head Sales", 20000);
		
		Employee headMarketing = new Employee("Marge", "Head Marketing", 20000);
		
		Employee salesExecutive1 = new Employee("Maggie", "Sales", 15000);
		Employee salesExecutive2 = new Employee("Santa's Little Helper", "Sales", 15000);

		Employee clerk1 = new Employee("Lisa", "Marketing", 15000);
		Employee clerk2 = new Employee("Bart", "Marketing", 15000);
		
		CEO.add(headSales);
		CEO.add(headMarketing);
		
		headSales.add(salesExecutive1);
		headSales.add(salesExecutive2);
		
		headMarketing.add(clerk1);
		headMarketing.add(clerk2);
		
		printEmployees(CEO);
	}
	
	//Loops through tree and prints out every branch.
	private static void printEmployees(Employee employee){
		
		System.out.println(employee.toString());
		
		if(!employee.getSubordinates().isEmpty()){
			for(Employee employee1 : employee.getSubordinates()){
				printEmployees(employee1);
			}
		}
	}
}

class Employee{
	private String name;
	private String dept;
	private int salary;
	private List<Employee> subordinates;
	
	public Employee(String name, String dept, int salary){
		this.name = name;
		this.dept = dept;
		this.salary = salary;
		
		subordinates = new ArrayList<Employee>();
	}
	
	public void add(Employee e){
		subordinates.add(e);
	}
	
	public void remove(Employee e){
		subordinates.remove(e);
	}
	
	public List<Employee> getSubordinates(){
		return subordinates;
	}
	
	public String toString(){
		return ("Employee :[ Name : " + name + ", dept : " + dept + ", salary :" + salary+" ]");
	} 
}