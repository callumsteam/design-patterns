package creational.prototype;

import java.util.Hashtable;

public class PrototypePatternDemo {
	public static void main(String[] args){
		ShapeCache.loadCache();
		
		Shape clone1 = ShapeCache.getShape("1");
		System.out.println("Shape : " + clone1.getType());
		
		Shape clone2 = ShapeCache.getShape("2");
		System.out.println("Shape : " + clone2.getType());
		
		Shape clone3 = ShapeCache.getShape("3");
		System.out.println("Shape : " + clone3.getType());
	}
}

class ShapeCache{
	private static Hashtable<String, Shape> shapeMap = new Hashtable<String, Shape>();
	
	public static Shape getShape(String shapeId){
		Shape cachedShape = shapeMap.get(shapeId);
		return (Shape) cachedShape.clone();
	}
	
	/* 
	 * Create objects to load into cache.
	 * In real world these should be
	 * expensive to create 
	 * e.g. require database reads.
	 */
	public static void loadCache(){
		Circle circle = new Circle();
		circle.setId("1");
		shapeMap.put(circle.getId(), circle);
		
		Square square = new Square();
		square.setId("2");
		shapeMap.put(square.getId(), square);
		
		Rectangle rectangle = new Rectangle();
		rectangle.setId("3");
		shapeMap.put(rectangle.getId(), rectangle);
	}
}

abstract class Shape implements Cloneable{
	private String id;
	protected String type;
	
	abstract void draw();
	
	public String getType(){
		return type;
	}
	
	public String getId(){
		return id;
	}
	
	public void setId(String id){
		this.id = id;
	}
	
	@Override
	public Object clone(){
		Object clone = null;
		
		try{
			clone = super.clone();
			
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
	    }
		
		return clone;
	}
}

class Rectangle extends Shape{
	public Rectangle(){
		type = "Rectangle";
	}
	
	@Override
	public void draw() {
		System.out.println("Inside Rectangle::draw() method.");
	}
}

class Circle extends Shape{
	public Circle(){
		type = "Circle";
	}
	
	@Override
	public void draw() {
		System.out.println("Inside Circle::draw() method.");
	}
}

class Square extends Shape{
	public Square(){
		type = "Square";
	}
	
	@Override
	public void draw() {
		System.out.println("Inside Square::draw() method.");
	}
}