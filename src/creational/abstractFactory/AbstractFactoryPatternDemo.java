package creational.abstractFactory;

public class AbstractFactoryPatternDemo {
	public static void main(String[] args){		
		//Uses factory producer class to get a shapefactory object
		AbstractFactory shapeFactory = FactoryProducer.getFactory(Choice.SHAPE);
		
		//Get a Circle shape object
		Shape circle = shapeFactory.getShape(Shapes.CIRCLE);
		circle.draw();
		
		//Get a Square shape object
		Shape square = shapeFactory.getShape(Shapes.SQUARE);
		square.draw();
		
		//Uses factory producer class to get a colourfactory object
		AbstractFactory colourFactory = FactoryProducer.getFactory(Choice.COLOUR);
		
		//Get a Red colour object
		Colour red = colourFactory.getColour(Colours.RED);
		red.fill();
		
		//Get Green colour object
		Colour green = colourFactory.getColour(Colours.GREEN);
		green.fill();
	}
}

/*
 * Shape interface used by all shape classes.
 * Type returned by the shapeFactory.
 */
interface Shape{
	void draw();
}

class Rectangle implements Shape{
	@Override
	public void draw(){
		System.out.println("Inside Rectangle::draw() method.");
	}
}

//Square - One of the shape classes
class Square implements Shape{
	@Override
	public void draw(){
		System.out.println("Inside Square::draw() method.");
	}
}

//Circle - One of the shape classes
class Circle implements Shape{
	@Override
	public void draw(){
		System.out.println("Inside Circle::draw() method.");
	}
}

//Enum list of all shapes type, used in the ShapeFactory to determine the shape object created.
enum Shapes{
	CIRCLE,
	SQUARE,
	RECTANGLE
}

/*
 * Colour interface used by all colour classes.
 * Type returned by the colourFactory.
 */
interface Colour{
	void fill();
}

//Red - One of the colour classes
class Red implements Colour{
	@Override
	public void fill(){
		System.out.println("Inside Red::fill() method.");
	}
}

//Red - One of the colour classes
class Green implements Colour{
	@Override
	public void fill(){
		System.out.println("Inside Green::fill() method.");
	}
}

//Blue - One of the colour classes
class Blue implements Colour{
	@Override
	public void fill(){
		System.out.println("Inside Blue::fill() method.");
	}
}

//Enum list of all colour type, used in the ColourFactory to determine the shape object created.
enum Colours{
	RED,
	GREEN,
	BLUE
}

/*
 * Abstract class which is extended from the colour and shape classes.
 */
abstract class AbstractFactory{
	abstract Shape getShape(Shapes shape);
	abstract Colour getColour(Colours colour);
}

/*
 * Factory class which builds one of the shape objects, in the getShape method.
 * Then returns the object cast as the shape interface, so the overridden draw method is still exposed.
 */
class ShapeFactory extends AbstractFactory{
	@Override
	Shape getShape(Shapes shape){
		switch(shape){
			case CIRCLE:
				return new Circle();
			
			case RECTANGLE:
				return new Rectangle();
				
			case SQUARE:
			default:
				return new Square();
		}
	}
	
	@Override
	Colour getColour(Colours colour){
		return null;
	}
}

/*
 * Factory class which builds one of the colour objects, in the getColour method.
 * Then returns the object cast as the shape interface, so the overridden fill method is still exposed.
 */
class ColourFactory extends AbstractFactory{
	@Override
	Shape getShape(Shapes shape){
		return null;
	}
	
	@Override
	Colour getColour(Colours colour){
		switch(colour){
			case RED:
				return new Red();
			
			case GREEN:
				return new Green();
				
			case BLUE:
			default:
				return new Blue();
		}
	}
}

/*
 * Enum list of the 2 object types, which can be rpoduced by the AbstractFactory.
 * Used in the FactoryProducer to determine which factory to return.
 */
enum Choice{
	SHAPE,
	COLOUR
}

/*
 * FactoryProducer, return specific factory depending on choice passed in.
 */
class FactoryProducer{
	public static AbstractFactory getFactory(Choice choice){
		switch(choice){
			case COLOUR:
				return new ColourFactory();
			
			case SHAPE:
			default:
				return new ShapeFactory();
		}
	}
}