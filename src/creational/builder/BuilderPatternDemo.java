package creational.builder;

import java.util.ArrayList;
import java.util.List;


public class BuilderPatternDemo {
	public static void main(String[] args){
		MealBuilder mealBuilder = new MealBuilder();
		
		Meal vegMeal = mealBuilder.prepareVegMeal();
		System.out.println("Veggie meal:");
		vegMeal.showItems();
		System.out.println("Total price: "+vegMeal.getCost());
		
		Meal nonVegMeal = mealBuilder.prepareNonVegMeal();
		System.out.println("\n\nNon-veg meal:");
		nonVegMeal.showItems();
		System.out.println("Total price: "+nonVegMeal.getCost());
	}
}

class MealBuilder{
	public Meal prepareVegMeal(){
		Meal meal = new Meal();
		meal.addItem(new VeggieBurger());
		meal.addItem(new Water());
		
		return meal;
	}
	
	public Meal prepareNonVegMeal(){
		Meal meal = new Meal();
		meal.addItem(new ChickenBurger());
		meal.addItem(new Coke());
		
		return meal;
	}
}

class Meal{
	private List<Item> items = new ArrayList<Item>();
	
	public void addItem(Item i){
		items.add(i);
	}
	
	public float getCost(){
		float price = 0.00f;
		
		for(Item i : items){
			price += i.price();
		}
		
		return price;
	}
	
	public void showItems(){
		for(Item i : items){
			System.out.print("Item : " + i.name());
	        System.out.print(", Packing : " + i.packing().pack());
	        System.out.println(", Price : " + i.price());
		}
	}
	
}

interface Item{
	public String name();
	public Packing packing();
	public float price();
}

interface Packing{
	public String pack();
}

class Wrapper implements Packing{
	@Override
	public String pack(){
		return "Wrapper";
	}
}

class Bottle implements Packing{
	@Override
	public String pack(){
		return "Bottle";
	}
}

abstract class Burger implements Item{
	@Override
	public Packing packing(){
		return new Wrapper();
	}
}

abstract class Drink implements Item{
	@Override
	public Packing packing(){
		return new Bottle();
	}
}

class VeggieBurger extends Burger{

	@Override
	public String name() {
		return "Veggie Burger";
	}

	@Override
	public float price() {
		return 2.25f;
	}
}

class ChickenBurger extends Burger{
	@Override
	public String name() {
		return "Chicken Burger";
	}
	
	@Override
	public float price() {
		return 2.99f;
	}
}

class Coke extends Drink{

	@Override
	public String name() {
		return "Coke";
	}

	@Override
	public float price() {
		return 1.99f;
	}
}

class Water extends Drink{

	@Override
	public String name() {
		return "Water";
	}

	@Override
	public float price() {
		return 0.99f;
	}
}