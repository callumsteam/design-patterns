package creational.singleton;

public class SingletonPatternDemo {
	public static void main(String[] args){
	    //illegal construct
	    //Compile Time Error: The constructor SingleObject() is not visible
	    //SingleObject object = new SingleObject();
		
		//Have to use the getInstance method to get the only instance available
		SingleObject obj = SingleObject.getInstance();
		
		obj.showMessage();
	}
}

class SingleObject{
	//Create an instance of SingleObject
	private static SingleObject instance = new SingleObject();
	
	//Make constructor private, so the class cannot be instantiated.
	private SingleObject(){}
	
	//Get the only instance available
	public static SingleObject getInstance(){
		return instance;
	}
	
	public void showMessage(){
		System.out.println("Hello World!");
	}
}