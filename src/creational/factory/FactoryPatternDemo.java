package creational.factory;

import java.util.ArrayList;

public class FactoryPatternDemo {
	//Tests all options for the ShapeFactory, getShape method.
	public static void main(String[] args){
		ShapeFactory shapeFactory = new ShapeFactory();
		
		//Shape object to hold object returned by factory.
		Shape shape;
		
		//Adds all items from Shapes enum to list.
		ArrayList<Shapes> shapesList = new ArrayList<Shapes>();
		shapesList.add(Shapes.CIRCLE);
		shapesList.add(Shapes.RECTANGLE);
		shapesList.add(Shapes.SQUARE);
		
		//Loops through shapesList and creates object for each, using the factory.
		for(Shapes shape1 : shapesList){
			shape = shapeFactory.getShape(shape1);
			shape.draw();			
		}
	}
}

/*
 * Shape interface used by all shape classes.
 * Type returned by the factory.
 */
interface Shape{
	void draw();
}

// Rectangle - One of the shape classes
class Rectangle implements Shape{
	@Override
	public void draw(){
		System.out.println("Inside Rectangle::draw() method.");
	}
}

//Square - One of the shape classes
class Square implements Shape{
	@Override
	public void draw(){
		System.out.println("Inside Square::draw() method.");
	}
}

//Circle - One of the shape classes
class Circle implements Shape{
	@Override
	public void draw(){
		System.out.println("Inside Circle::draw() method.");
	}
}

/*
 * Factory class which builds one of the shape objects, in the getShape method.
 * Then returns the object cast as the shape interface, so the overridden draw method is still exposed.
 */
class ShapeFactory{
	//Returns object of type shape, depending on the enum argument passed in.
	public Shape getShape(Shapes shape){
		switch(shape){
			case CIRCLE:
				return new Circle();
			
			case RECTANGLE:
				return new Rectangle();
				
			case SQUARE:
			default:
				return new Square();
		}
	}
}

//Enum list of all shapes type, used in the ShapeFactory to determine the shape object created.
enum Shapes{
	CIRCLE,
	SQUARE,
	RECTANGLE
}